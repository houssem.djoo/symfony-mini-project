<?php

namespace App\Entity;

use App\Repository\InterventionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InterventionRepository::class)
 */
class Intervention
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $dateInterv;

    /**
     * @ORM\OneToMany(targetEntity=Client::class, mappedBy="interventions")
     */
    private $codeClt;

    /**
     * @ORM\OneToMany(targetEntity=Produit::class, mappedBy="interventions")
     */
    private $referencePd;

    /**
     * @ORM\OneToMany(targetEntity=Technicien::class, mappedBy="interventions")
     */
    private $codeTech;

    public function __construct()
    {
        $this->codeClt = new ArrayCollection();
        $this->referencePd = new ArrayCollection();
        $this->codeTech = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateInterv(): ?\DateTimeInterface
    {
        return $this->dateInterv;
    }

    public function setDateInterv(\DateTimeInterface $dateInterv): self
    {
        $this->dateInterv = $dateInterv;

        return $this;
    }

    /**
     * @return Collection|Client[]
     */
    public function getCodeClt(): Collection
    {
        return $this->codeClt;
    }

    public function addCodeClt(Client $codeClt): self
    {
        if (!$this->codeClt->contains($codeClt)) {
            $this->codeClt[] = $codeClt;
            $codeClt->setInterventions($this);
        }

        return $this;
    }

    public function removeCodeClt(Client $codeClt): self
    {
        if ($this->codeClt->removeElement($codeClt)) {
            // set the owning side to null (unless already changed)
            if ($codeClt->getInterventions() === $this) {
                $codeClt->setInterventions(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Produit[]
     */
    public function getReferencePd(): Collection
    {
        return $this->referencePd;
    }

    public function addReferencePd(Produit $referencePd): self
    {
        if (!$this->referencePd->contains($referencePd)) {
            $this->referencePd[] = $referencePd;
            $referencePd->setInterventions($this);
        }

        return $this;
    }

    public function removeReferencePd(Produit $referencePd): self
    {
        if ($this->referencePd->removeElement($referencePd)) {
            // set the owning side to null (unless already changed)
            if ($referencePd->getInterventions() === $this) {
                $referencePd->setInterventions(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Technicien[]
     */
    public function getCodeTech(): Collection
    {
        return $this->codeTech;
    }

    public function addCodeTech(Technicien $codeTech): self
    {
        if (!$this->codeTech->contains($codeTech)) {
            $this->codeTech[] = $codeTech;
            $codeTech->setInterventions($this);
        }

        return $this;
    }

    public function removeCodeTech(Technicien $codeTech): self
    {
        if ($this->codeTech->removeElement($codeTech)) {
            // set the owning side to null (unless already changed)
            if ($codeTech->getInterventions() === $this) {
                $codeTech->setInterventions(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getDateInterv()->format('Y-m-d');
    }
}
