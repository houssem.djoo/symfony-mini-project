<?php

namespace App\Entity;

use App\Repository\TechnicienRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TechnicienRepository::class)
 */
class Technicien
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $codeTech;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\ManyToOne(targetEntity=Intervention::class, inversedBy="codeTech")
     */
    private $interventions;

    public function getCodeTech(): ?int
    {
        return $this->codeTech;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function __toString()
    {
        return $this->prenom . ' ' . $this->nom;
    }

    public function getInterventions(): ?Intervention
    {
        return $this->interventions;
    }

    public function setInterventions(?Intervention $interventions): self
    {
        $this->interventions = $interventions;

        return $this;
    }
}
