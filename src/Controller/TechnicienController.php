<?php

namespace App\Controller;

use App\Entity\Technicien;
use App\Form\TechnicienType;
use App\Repository\TechnicienRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/technicien")
 */
class TechnicienController extends AbstractController
{
    /**
     * @Route("/", name="technicien_index", methods={"GET"})
     */
    public function index(TechnicienRepository $technicienRepository): Response
    {
        return $this->render('technicien/index.html.twig', [
            'techniciens' => $technicienRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="technicien_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $technicien = new Technicien();
        $form = $this->createForm(TechnicienType::class, $technicien);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($technicien);
            $entityManager->flush();

            return $this->redirectToRoute('technicien_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('technicien/new.html.twig', [
            'technicien' => $technicien,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{codeTech}", name="technicien_show", methods={"GET"})
     */
    public function show(Technicien $technicien): Response
    {
        return $this->render('technicien/show.html.twig', [
            'technicien' => $technicien,
        ]);
    }

    /**
     * @Route("/{codeTech}/edit", name="technicien_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Technicien $technicien, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(TechnicienType::class, $technicien);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('technicien_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('technicien/edit.html.twig', [
            'technicien' => $technicien,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{codeTech}", name="technicien_delete", methods={"POST"})
     */
    public function delete(Request $request, Technicien $technicien, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$technicien->getCodeTech(), $request->request->get('_token'))) {
            $entityManager->remove($technicien);
            $entityManager->flush();
        }

        return $this->redirectToRoute('technicien_index', [], Response::HTTP_SEE_OTHER);
    }
}
